# -*- coding: utf-8 -*-
# @Author: Bartuccio Antoine
# @Date:   2021-03-05 23:35:57
# @Last Modified by:   Sli
# @Last Modified time: 2023-04-17 12:32:23

# conan export-pkg . SharpAttack+conan_packages/stable

from conans import ConanFile
import os


class MicroXRCEDDSGenConan(ConanFile):
    name = "Micro-XRCE-DDS-Gen"
    version = "1.1.1"
    license = "Apache-2.0"
    author = "eProsima"
    url = "https://github.com/eProsima/Micro-XRCE-DDS-Gen"
    description = "DDS Generator"
    settings = "arch"

    def requirements(self):
        if self.settings.arch == "x86_64":
            self.requires("zulu-openjdk/11.0.8")

    def package(self):
        self.copy("*", src="bin", dst="bin", keep_path=True)

    def package_info(self):
        self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))
