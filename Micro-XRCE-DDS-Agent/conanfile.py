# -*- coding: utf-8 -*-
# @Author: Bartuccio Antoine
# @Date:   2021-04-23 22:29:58
# @Last Modified by:   klmp200
# @Last Modified time: 2021-08-27 17:41:14

# conan create . SharpAttack+conan_packages/stable

import os
from conans import ConanFile, CMake, tools

class MicroXRCEDDSAgentConan(ConanFile):
    name = "Micro-XRCE-DDS-Agent"
    version = "2.0.0-1"
    license = "Apache-2.0"
    author = "eProsima"
    url = "https://github.com/eProsima/Micro-XRCE-DDS-Agent"
    settings = "os", "compiler", "build_type"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    generators = "cmake"

    def configure(self):
        self.folder_name = "micro-xrce-dds-agent-2.0.0"

    def source(self):
        git = tools.Git(folder=self.folder_name)
        git.clone("https://github.com/eProsima/Micro-XRCE-DDS-Agent.git")
        git.checkout("v2.0.0")


    def build(self):
        if self.settings.os != "Windows":
            tools.patch(self.folder_name, patch_string=unix_patch)
        cmake = CMake(self)
        cmake.verbose = True
        cmake.configure(source_dir=self.folder_name)
        cmake.build()

    def package(self):
        self.copy("MicroXRCEAgent", src=".", dst="bin", keep_path=True)

    def package_info(self):
        self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))


unix_patch = r"""
From 4ab71ede8784aa43473fedc198b70759304052ab Mon Sep 17 00:00:00 2001
From: klmp200 <klmp200@klmp200.net>
Date: Fri, 23 Apr 2021 17:46:03 +0200
Subject: [PATCH] Wopiti Wapiti, your code is my property

---
 CMakeLists.txt                                | 46 ++++++-------------
 .../transport/endpoint/CustomEndPoint.hpp     |  1 +
 .../transport/serial/baud_rate_table_linux.h  | 17 ++++++-
 .../stream_framing/StreamFramingProtocol.hpp  |  2 +
 include/uxr/agent/utils/ArgumentParser.hpp    |  4 +-
 src/cpp/processor/Processor.cpp               |  2 -
 src/cpp/transport/Server.cpp                  |  3 --
 7 files changed, 36 insertions(+), 39 deletions(-)

diff --git a/CMakeLists.txt b/CMakeLists.txt
index 4c464f6..cb646a9 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -49,9 +49,7 @@ if(NOT UAGENT_CED_PROFILE)
     set(UAGENT_P2P_PROFILE OFF)
 endif()
 
-if((CMAKE_SYSTEM_NAME STREQUAL "") AND (NOT CMAKE_HOST_SYSTEM_NAME STREQUAL "Linux"))
-    set(UAGENT_P2P_PROFILE OFF)
-endif()
+set(UAGENT_P2P_PROFILE OFF)
 
 set(UAGENT_CONFIG_RELIABLE_STREAM_DEPTH        16       CACHE STRING "Reliable streams depth.")
 set(UAGENT_CONFIG_BEST_EFFORT_STREAM_DEPTH     16       CACHE STRING "Best-effort streams depth.")
@@ -148,27 +146,17 @@ endif()
 # Sources
 ###############################################################################
 # Check platform.
-if(CMAKE_SYSTEM_NAME STREQUAL "Linux")
-    set(TRANSPORT_SRCS
-        src/cpp/transport/udp/UDPv4AgentLinux.cpp
-        src/cpp/transport/udp/UDPv6AgentLinux.cpp
-        src/cpp/transport/tcp/TCPv4AgentLinux.cpp
-        src/cpp/transport/tcp/TCPv6AgentLinux.cpp
-        src/cpp/transport/serial/SerialAgentLinux.cpp
-        src/cpp/transport/serial/TermiosAgentLinux.cpp
-        src/cpp/transport/serial/PseudoTerminalAgentLinux.cpp
-        $<$<BOOL:${UAGENT_DISCOVERY_PROFILE}>:src/cpp/transport/discovery/DiscoveryServerLinux.cpp>
-        $<$<BOOL:${UAGENT_P2P_PROFILE}>:src/cpp/transport/p2p/AgentDiscovererLinux.cpp>
-        )
-elseif(CMAKE_SYSTEM_NAME STREQUAL "Windows")
-    set(TRANSPORT_SRCS
-        src/cpp/transport/udp/UDPv4AgentWindows.cpp
-        src/cpp/transport/udp/UDPv6AgentWindows.cpp
-        src/cpp/transport/tcp/TCPv4AgentWindows.cpp
-        src/cpp/transport/tcp/TCPv6AgentWindows.cpp
-        $<$<BOOL:${UAGENT_DISCOVERY_PROFILE}>:src/cpp/transport/discovery/DiscoveryServerWindows.cpp>
-        )
-endif()
+set(TRANSPORT_SRCS
+    src/cpp/transport/udp/UDPv4AgentLinux.cpp
+    src/cpp/transport/udp/UDPv6AgentLinux.cpp
+    src/cpp/transport/tcp/TCPv4AgentLinux.cpp
+    src/cpp/transport/tcp/TCPv6AgentLinux.cpp
+    src/cpp/transport/serial/SerialAgentLinux.cpp
+    src/cpp/transport/serial/TermiosAgentLinux.cpp
+    src/cpp/transport/serial/PseudoTerminalAgentLinux.cpp
+    $<$<BOOL:${UAGENT_DISCOVERY_PROFILE}>:src/cpp/transport/discovery/DiscoveryServerLinux.cpp>
+    $<$<BOOL:${UAGENT_P2P_PROFILE}>:src/cpp/transport/p2p/AgentDiscovererLinux.cpp>
+    )
 
 # Set source files
 set(SRCS
@@ -194,7 +182,6 @@ set(SRCS
     src/cpp/utils/ArgumentParser.cpp
     src/cpp/transport/Server.cpp
     src/cpp/transport/stream_framing/StreamFramingProtocol.cpp
-    src/cpp/transport/custom/CustomAgent.cpp
     ${TRANSPORT_SRCS}
     $<$<BOOL:${UAGENT_DISCOVERY_PROFILE}>:src/cpp/transport/discovery/DiscoveryServer.cpp>
     $<$<BOOL:${UAGENT_FAST_PROFILE}>:src/cpp/types/TopicPubSubType.cpp>
@@ -292,7 +279,7 @@ target_link_libraries(${PROJECT_NAME}
         $<$<BOOL:${UAGENT_FAST_PROFILE}>:fastrtps>
         $<$<BOOL:${UAGENT_P2P_PROFILE}>:microxrcedds_client>
         $<$<BOOL:${UAGENT_P2P_PROFILE}>:microcdr>
-        $<$<PLATFORM_ID:Linux>:pthread>
+        pthread
     )
 
 target_include_directories(${PROJECT_NAME} BEFORE
@@ -315,8 +302,7 @@ if(UAGENT_BUILD_EXECUTABLE)
     target_link_libraries(MicroXRCEAgent
         PRIVATE
             ${PROJECT_NAME}
-            $<$<BOOL:$<PLATFORM_ID:Linux>>:rt>
-            $<$<BOOL:$<PLATFORM_ID:Linux>>:dl>
+            dl
         )
 
     set_target_properties(MicroXRCEAgent PROPERTIES
@@ -371,9 +357,7 @@ if(UAGENT_BUILD_TESTS)
     add_subdirectory(test/unittest/utils)
     add_subdirectory(test/unittest/types)
     add_subdirectory(test/unittest/client/session/stream)
-    if(CMAKE_SYSTEM_NAME STREQUAL "Linux")
-        add_subdirectory(test/unittest/transport/serial)
-    endif()
+    add_subdirectory(test/unittest/transport/serial)
 endif()
 
 ###############################################################################
diff --git a/include/uxr/agent/transport/endpoint/CustomEndPoint.hpp b/include/uxr/agent/transport/endpoint/CustomEndPoint.hpp
index 96483a4..8c12d23 100644
--- a/include/uxr/agent/transport/endpoint/CustomEndPoint.hpp
+++ b/include/uxr/agent/transport/endpoint/CustomEndPoint.hpp
@@ -348,6 +348,7 @@ public:
         return false;
     }
 
+
     /**
      * @brief Operator << overload for ostream operations.
      * @param os The ostream object to which the output is sent.
diff --git a/include/uxr/agent/transport/serial/baud_rate_table_linux.h b/include/uxr/agent/transport/serial/baud_rate_table_linux.h
index 44655da..7204ff1 100644
--- a/include/uxr/agent/transport/serial/baud_rate_table_linux.h
+++ b/include/uxr/agent/transport/serial/baud_rate_table_linux.h
@@ -20,6 +20,21 @@
 #include <string.h>
 #include <termios.h>
 
+#ifdef __APPLE__
+#define B460800 460800
+#define B500000 500000
+#define B576000 576000
+#define B921600 921600
+#define B1000000 1000000
+#define B1152000 1152000
+#define B1500000 1500000
+#define B2000000 2000000
+#define B2500000 2500000
+#define B3000000 3000000
+#define B3500000 3500000
+#define B4000000 4000000
+#endif
+
 inline
 speed_t getBaudRate(const char* baudrate_str)
 {
@@ -151,7 +166,7 @@ speed_t getBaudRate(const char* baudrate_str)
     else
     {
         speed_t custom_baud_rate = (speed_t)atoi(baudrate_str);
-        printf("Warning: Custom baud rate set to: %d\n", custom_baud_rate);
+        printf("Warning: Custom baud rate set to: %lu\n", custom_baud_rate);
         rv = custom_baud_rate;
     }
     return rv;
diff --git a/include/uxr/agent/transport/stream_framing/StreamFramingProtocol.hpp b/include/uxr/agent/transport/stream_framing/StreamFramingProtocol.hpp
index 8e402b4..e1c4ce6 100644
--- a/include/uxr/agent/transport/stream_framing/StreamFramingProtocol.hpp
+++ b/include/uxr/agent/transport/stream_framing/StreamFramingProtocol.hpp
@@ -24,6 +24,8 @@
 #ifdef _WIN32
 #include <BaseTsd.h>
 typedef SSIZE_T ssize_t;
+#else
+#include <sys/types.h>
 #endif
 
 namespace eprosima {
diff --git a/src/cpp/processor/Processor.cpp b/src/cpp/processor/Processor.cpp
index 8bc63f9..6bfc6fd 100644
--- a/src/cpp/processor/Processor.cpp
+++ b/src/cpp/processor/Processor.cpp
@@ -24,7 +24,6 @@
 #include <uxr/agent/transport/endpoint/IPv4EndPoint.hpp>
 #include <uxr/agent/transport/endpoint/IPv6EndPoint.hpp>
 #include <uxr/agent/transport/endpoint/SerialEndPoint.hpp>
-#include <uxr/agent/transport/endpoint/CustomEndPoint.hpp>
 
 namespace eprosima {
 namespace uxr {
@@ -896,7 +895,6 @@ void Processor<EndPoint>::check_heartbeats()
 template class Processor<IPv4EndPoint>;
 template class Processor<IPv6EndPoint>;
 template class Processor<SerialEndPoint>;
-template class Processor<CustomEndPoint>;
 
 } // namespace uxr
 } // namespace eprosima
diff --git a/src/cpp/transport/Server.cpp b/src/cpp/transport/Server.cpp
index 1df335c..769ce02 100644
--- a/src/cpp/transport/Server.cpp
+++ b/src/cpp/transport/Server.cpp
@@ -21,7 +21,6 @@
 #include <uxr/agent/transport/endpoint/IPv4EndPoint.hpp>
 #include <uxr/agent/transport/endpoint/IPv6EndPoint.hpp>
 #include <uxr/agent/transport/endpoint/SerialEndPoint.hpp>
-#include <uxr/agent/transport/endpoint/CustomEndPoint.hpp>
 
 #include <functional>
 
@@ -33,7 +32,6 @@ namespace uxr {
 extern template class Processor<IPv4EndPoint>;
 extern template class Processor<IPv6EndPoint>;
 extern template class Processor<SerialEndPoint>;
-extern template class Processor<CustomEndPoint>;
 
 template<typename EndPoint>
 Server<EndPoint>::Server(Middleware::Kind middleware_kind)
@@ -265,7 +263,6 @@ void Server<EndPoint>::error_handler_loop()
 template class Server<IPv4EndPoint>;
 template class Server<IPv6EndPoint>;
 template class Server<SerialEndPoint>;
-template class Server<CustomEndPoint>;
 
 } // namespace uxr
 } // namespace eprosima
-- 
2.24.3 (Apple Git-128)



"""