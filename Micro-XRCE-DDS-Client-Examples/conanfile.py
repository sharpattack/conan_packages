# -*- coding: utf-8 -*-
# @Author: Bartuccio Antoine
# @Date:   2021-04-23 22:29:58
# @Last Modified by:   klmp200
# @Last Modified time: 2021-08-27 22:05:42

# conan create . SharpAttack+conan_packages/stable

import os
from conans import ConanFile, CMake, tools
from conans.client.tools.win import unix_path

class MicroXRCEDDSClientConan(ConanFile):
    name = "Micro-XRCE-DDS-Client-Examples"
    version = "2.0.0"
    license = "Apache-2.0"
    author = "eProsima"
    url = "https://github.com/eProsima/Micro-XRCE-DDS-Client"
    settings = "os", "compiler", "build_type"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    generators = "cmake"

    def configure(self):
        self.folder_name = "micro-xrce-dds-client-2.0.0"

    def source(self):
        git = tools.Git(folder=self.folder_name)
        git.clone("https://github.com/eProsima/Micro-XRCE-DDS-Client.git")
        git.checkout("v2.0.0")


    def build(self):
        if self.settings.os != "Windows":
            tools.patch(self.folder_name, patch_string=unix_patch)
        cmake = CMake(self)
        cmake.definitions["UCLIENT_BUILD_EXAMPLES"] = "ON"
        cmake.verbose = True
        cmake.configure(source_dir=self.folder_name)
        cmake.build()

    def package(self):
        self.copy("SubscribeHelloWorldClientBestEffort", src="examples/SubscribeHelloWorldBestEffort/", dst="bin")
        self.copy("ShapeDemoClient", src="examples/ShapesDemo/", dst="bin")
        self.copy("RequestAdder", src="examples/RequestAdder/", dst="bin")
        self.copy("ContinuousFragment", src="examples/ContinuousFragment/", dst="bin")
        self.copy("CustomTransports", src="examples/CustomTransports/", dst="bin")
        self.copy("UDP/Ping", src="examples/PingAgent/", dst="bin")
        self.copy("TCP/Ping", src="examples/PingAgent/", dst="bin")
        self.copy("Serial/Ping", src="examples/PingAgent/", dst="bin")
        self.copy("Discovery", src="examples/Discovery/", dst="bin")
        self.copy("SubscribeHelloWorldClient", src="examples/SubscribeHelloWorld/", dst="bin")
        self.copy("MultiSessionHelloWorld", src="examples/MultiSessionHelloWorld/", dst="bin")
        self.copy("PublishHelloWorldClient", src="examples/PublishHelloWorld/", dst="bin")
        self.copy("PublisherClient", src="examples/Deployment/", dst="bin")
        self.copy("SubscriberClient", src="examples/Deployment/", dst="bin")
        self.copy("ConfiguratorClient", src="examples/Deployment/", dst="bin")
        self.copy("PublishHelloWorldClientP2P", src="examples/PublishHelloWorldP2P/", dst="bin")
        self.copy("TimeSync", src="examples/TimeSync/", dst="bin")
        self.copy("PublishHelloWorldClientBestEffort", src="examples/PublishHelloWorldBestEffort/", dst="bin")
        self.copy("ReplyAdder", src="examples/ReplyAdder/", dst="bin")
        self.copy("TimeSyncWithCb", src="examples/TimeSyncWithCb/", dst="bin")

    def package_info(self):
        self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))

unix_patch = r"""
From 56121373055d588c9dc775e1c13a0cd28ee21572 Mon Sep 17 00:00:00 2001
From: klmp200 <klmp200@klmp200.net>
Date: Fri, 27 Aug 2021 21:49:16 +0200
Subject: [PATCH] Wopiti Wapiti, your code is my property

---
 CMakeLists.txt | 16 +---------------
 1 file changed, 1 insertion(+), 15 deletions(-)

diff --git a/CMakeLists.txt b/CMakeLists.txt
index 622ebec..cc56921 100755
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -125,21 +125,7 @@ endforeach()
 # Sources
 ###############################################################################
 # Check platform.
-if(CMAKE_SYSTEM_NAME STREQUAL "Linux")
-    set(UCLIENT_PLATFORM_LINUX ON)
-elseif(CMAKE_SYSTEM_NAME STREQUAL "Windows")
-    set(UCLIENT_PLATFORM_WINDOWS ON)
-elseif(CMAKE_SYSTEM_NAME STREQUAL "Generic")
-    if(PLATFORM_NAME STREQUAL "nuttx")
-        set(UCLIENT_PLATFORM_NUTTX ON)
-    elseif(PLATFORM_NAME STREQUAL "zephyr")
-        set(UCLIENT_PLATFORM_ZEPHYR ON)
-    elseif(PLATFORM_NAME STREQUAL "LwIP")
-        set(UCLIENT_PLATFORM_POSIX_NOPOLL ON)
-    elseif(PLATFORM_NAME STREQUAL "FreeRTOS_Plus_TCP")
-        set(UCLIENT_PLATFORM_FREERTOS_PLUS_TCP ON)
-    endif()
-endif()
+set(UCLIENT_PLATFORM_LINUX ON)
 
 if(UCLIENT_PLATFORM_LINUX OR UCLIENT_PLATFORM_NUTTX OR UCLIENT_PLATFORM_ZEPHYR)
     set(UCLIENT_PLATFORM_POSIX ON)
-- 
2.30.1 (Apple Git-130)
"""