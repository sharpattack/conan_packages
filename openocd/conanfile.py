# conan create . SharpAttack+conan_packages/stable

import os
from conans import ConanFile, CMake, tools
from conans.errors import ConanInvalidConfiguration, ConanException


class OpenocdConan(ConanFile):
    name = "openocd"
    version = "0.11.0-1"
    license = "GNU-GPL-v2"
    author = "Openocd"
    url = "https://github.com/xpack-dev-tools/openocd-xpack/releases"
    description = "Open debugger and flasher for embeded devices"
    settings = "os", "arch"

    def configure(self):
        self.folder_name = "xpack-openocd-0.11.0-1"
        if self.settings.os == "Windows" and self.settings.arch == "x86":
            self.archive_name = "xpack-openocd-0.11.0-1-win32-ia32.zip"
            self.hash = "b556754ee621962d41f89b229115ecf5e067b6ae76e91e210d7f53d657769296"

        elif self.settings.os == "Windows" and self.settings.arch == "x86_64":
            self.archive_name = " xpack-openocd-0.11.0-1-win32-x64.zip"
            self.hash = "ec37f535265ce93953a874b10d07e8578f9814c206d8bce6a241ba7016297e4f"

        elif self.settings.os == "Linux" and self.settings.arch == "x86":
            self.archive_name = "xpack-openocd-0.11.0-1-linux-ia32.tar.gz"
            self.hash = "fb38cc36c31eccbfcb824684596fb16dde91f9c9a42a954d9c26e678cc1a5fc1"

        elif self.settings.os == "Linux" and self.settings.arch == "x86_64":
            self.archive_name = "xpack-openocd-0.11.0-1-linux-x64.tar.gz"
            self.hash = "5972fe70a274f054503dd519b68d3909b83f017b5b8dd2b59e84b3b72c9bc3e1"

        elif self.settings.os == "Linux" and self.settings.arch == "arm":
            self.archive_name = "xpack-openocd-0.11.0-1-linux-arm.tar.gz"
            self.hash = "24c5de0839b8c5cb3476d6fb7b9f528daba14b434a00d60ef71d4e4da3131262"

        elif self.settings.os == "Macos":
            self.archive_name = "xpack-openocd-0.11.0-1-darwin-x64.tar.gz"
            self.hash = "3e3719fd059d87f3433f1f6d8e37b8582e87ae6a168287eb32a85dbc0f2e1708"

        else:
            raise ConanInvalidConfiguration("Your system is not supported")

        self.download_url = "https://github.com/xpack-dev-tools/openocd-xpack/releases/download/v0.11.0-1/%s" % self.archive_name

    @property
    def gcc_folder_name(self):
        return "openocd-%s" % self.version 

    def build(self):
        self.output.info("Downloading openocd: %s" % self.download_url)
        tools.download(self.download_url, self.archive_name, sha256=self.hash)

        self.output.info("Extracting openocd")
        tools.unzip(self.archive_name)
        os.unlink(self.archive_name)

    def package(self):
        self.copy("*", src=self.folder_name, dst="", keep_path=True)

    def package_info(self):
        self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))
