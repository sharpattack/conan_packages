# -*- coding: utf-8 -*-
# @Author: Bartuccio Antoine
# @Date:   2021-02-12 23:31:42
# @Last Modified by:   klmp200
# @Last Modified time: 2021-08-01 18:37:53

# conan create . SharpAttack+conan_packages/stable

import os
from conans import ConanFile, CMake, tools
from conans.errors import ConanInvalidConfiguration, ConanException


class GccArmNoneEabiConan(ConanFile):
    name = "gcc-arm-none-eabi"
    version = "10-2020-q4-major"
    license = "GPL-v3"
    author = "ARM"
    url = "https://www.gnu.org/software/binutils/"
    settings = "os", "arch", "compiler"
    description = "ARM Toolchain"

    def configure(self):
        self.folder_name = "gcc-arm-none-eabi-10-2020-q4-major"
        if self.settings.os == "Windows":
            self.archive_name = "gcc-arm-none-eabi-10-2020-q4-major-win32.zip"
            self.download_url = "https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-rm/10-2020q4/gcc-arm-none-eabi-10-2020-q4-major-win32.zip"
            self.hash = "5ee6542a2af847934177bc8fa1294c0d"

        elif self.settings.os == "Linux" and self.settings.arch == "x86_64":
            self.archive_name = "gcc-arm-none-eabi-10-2020-q4-major-x86_64-linux.tar.bz2"
            self.download_url = "https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-rm/10-2020q4/gcc-arm-none-eabi-10-2020-q4-major-x86_64-linux.tar.bz2"
            self.hash = "8312c4c91799885f222f663fc81f9a31"

        elif self.settings.os == "Linux" and self.settings.arch == "arm":
            self.archive_name = "gcc-arm-none-eabi-10-2020-q4-major-aarch64-linux.tar.bz2"
            self.download_url = "https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-rm/10-2020q4/gcc-arm-none-eabi-10-2020-q4-major-aarch64-linux.tar.bz2"
            self.hash = "1c3b8944c026d50362eef1f01f329a8e"

        elif self.settings.os == "Macos":
            self.archive_name = "gcc-arm-none-eabi-10-2020-q4-major-mac.tar.bz2"
            self.download_url = "https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-rm/10-2020q4/gcc-arm-none-eabi-10-2020-q4-major-mac.tar.bz2"
            self.hash = "e588d21be5a0cc9caa60938d2422b058"

        else:
            raise ConanInvalidConfiguration("Your system is not supported")


    @property
    def gcc_folder_name(self):
        return "arm-none-eabi-%s" % self.version 

    def build(self):
        self.output.info("Downloading gcc: %s" % self.download_url)
        tools.download(self.download_url, self.archive_name, md5=self.hash)

        self.output.info("Extracting gcc")
        tools.unzip(self.archive_name)
        os.unlink(self.archive_name)

    def package(self):
        self.copy("*", src=self.folder_name, dst="", keep_path=True)

    def package_info(self):
        self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))
